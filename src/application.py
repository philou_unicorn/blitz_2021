#!/usr/bin/env python

import asyncio
import os
import websockets
import json

from bot import Bot
from bot_message import BotMessage, MessageType
from game_message import GameMessage, Crew
from game_command import UnitActionType
from strategy_aggressive import AggressiveStrategy
from map_cache import MapCache
from util import units_list_to_dict


async def run():
    uri = "ws://127.0.0.1:8765"

    async with websockets.connect(uri) as websocket:
        bot = Bot()
        if "TOKEN" in os.environ:
            await websocket.send(json.dumps({"type": "REGISTER", "token": os.environ["TOKEN"]}))
        else:
            await websocket.send(json.dumps({"type": "REGISTER", "crewName": "MyBot"}))

        await game_loop(websocket=websocket, bot=bot)


async def game_loop(websocket: websockets.WebSocketServerProtocol, bot: Bot):
    map_cache = MapCache()
    stratego = AggressiveStrategy()
    while True:
        try:
            message = await websocket.recv()
        except websockets.exceptions.ConnectionClosed:
            # Connection is closed, the game is probably over
            break

        game_message: GameMessage = GameMessage.from_json(message)
        mines_position = map_cache.get_mines(game_message.map, game_message.tick)

        stratego.set_game_message(game_message)

        my_crew: Crew = game_message.get_crews_by_id()[game_message.crewId]
        my_crew_dict = units_list_to_dict(my_crew.units)
        stratego.set_units_dict(my_crew_dict)

        stratego.handle_miner_and_cart_deaths(my_crew_dict)
        stratego.associate_miner_with_cart(my_crew)

        print(f"\Tick {game_message.tick}")
        print(f"\nError? {' '.join(my_crew.errors)}")

        # next_move: UnitActionType.MOVE = bot.get_next_move(game_message)
        moves = []
        # print(my_crew.units)
        for unit in my_crew.units:
            moves.append(stratego.get_next_move_for_unit(unit, my_crew.homeBase, mines_position, my_crew))
        buy_action = bot.buy(game_message, map_cache)
        act = moves + buy_action
        await websocket.send(BotMessage(type=MessageType.COMMAND, actions=act, tick=game_message.tick).to_json())


if __name__ == "__main__":
    asyncio.get_event_loop().run_until_complete(run())
