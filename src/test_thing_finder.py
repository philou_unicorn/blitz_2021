from unittest import TestCase
from game_message import Position, Map, Crew

from thing_finder import manathan_distance, find_closest

class ThingFinderTest(TestCase):
    def test_manathan(self):
        x1 = Position(10, 5)
        x2 = Position(1, 1)

        self.assertEquals(manathan_distance(x1, x2), 13)

    def test_find_closest(self):
        current_position = Position(12, 13)
        positions = [Position(2, 3), Position(5, 5), Position(10, 11), Position(20, 20), Position(30, 30)]
        all_tiles = []
        for i in range(40):
            horizontal_tiles = []
            for j in range(40):
                if Position(i, j) in positions:
                    horizontal_tiles.append("MINE")
                else:
                    horizontal_tiles.append("EMPTY")
            all_tiles.append(horizontal_tiles)
        map = Map(all_tiles, [])
        crews = []

        closest = find_closest(current_position, positions, map, crews)
        self.assertEquals(closest.x, 10)
        self.assertEquals(closest.y, 11)
