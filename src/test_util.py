from game_message import Position, Unit, TileType, UnitType, GameMessage, Crew, Position, Rules
from unittest import TestCase, skip
from util import units_list_to_dict

class UtilTest(TestCase):
    def test_units_to_dicts(self):

        cart1 = Unit(
          1,
          UnitType.CART,
          5,
          Position(1, 1),
          []
        )

        cart2 = Unit(
          2,
          UnitType.CART,
          5,
          Position(1, 1),
          []
        )

        cart3 = Unit(
          3,
          UnitType.CART,
          5,
          Position(1, 1),
          []
        )

        miner1 = Unit(
          4,
          UnitType.MINER,
          5,
          Position(1, 1),
          []
        )

        miner2 = Unit(
          5,
          UnitType.MINER,
          5,
          Position(1, 1),
          []
        )

        miner3 = Unit(
          6,
          UnitType.MINER,
          5,
          Position(1, 1),
          []
        )

        units = [cart1, cart2, cart3, miner1, miner2, miner3]

        self.assertEquals(units_list_to_dict(units), {
          1: cart1,
          2: cart2,
          3: cart3,
          4: miner1,
          5: miner2,
          6: miner3,
        })
