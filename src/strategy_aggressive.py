from game_message import Position, Unit, TileType, UnitType, GameMessage, Crew
from thing_finder import find_closest, find_tiles_of_type, get_unoccupied_near_closest, manathan_distance, find_unoccupied_closest
from game_command import UnitAction, UnitActionType
from typing import List, Dict
from util import find_unit, getClosestEnemyByType, getNextOutlawMovePositionDumb, IsAdjacentToEnemy

import random
import traceback

MINER_ID_KEY = "miner_id"
CART_ID_KEY = "cart_id"

class AggressiveStrategy:
    def __init__(self):
        self.game_message = None
        self.miners_carts_associated = set()
        self.miner_cart_pairs = {}
        self.units_dict = None

    def set_game_message(self, game_message: GameMessage):
        self.game_message = game_message

    def set_units_dict(self, my_crew_dict: Dict[str, Unit]):
        self.units_dict = my_crew_dict

    def handle_miner_and_cart_deaths(self, my_crew_dict: Dict[str, Unit]):
        temp_set = set(self.miners_carts_associated)
        for miner_cart_associated_id in temp_set:
            if my_crew_dict.get(miner_cart_associated_id) is None:
                miner_cart_pair = self.miner_cart_pairs[miner_cart_associated_id]
                miner_id = miner_cart_pair[MINER_ID_KEY]
                cart_id = miner_cart_pair[CART_ID_KEY]
                self.miners_carts_associated.remove(cart_id)
                self.miners_carts_associated.remove(miner_id)
                del self.miner_cart_pairs[miner_id]
                del self.miner_cart_pairs[cart_id]

    def associate_miner_with_cart(self, my_crew: Crew):
        miners = [unit for unit in my_crew.units if unit.type == UnitType.MINER]
        carts = [unit for unit in my_crew.units if unit.type == UnitType.CART]
        # TODO: handle deaths plz :)
        for miner in miners:
            if miner.id not in self.miners_carts_associated:
                for cart in carts:
                    if cart.id not in self.miners_carts_associated:
                        pair = {
                            MINER_ID_KEY: miner.id,
                            CART_ID_KEY: cart.id
                        }
                        self.miner_cart_pairs[miner.id] = pair
                        self.miner_cart_pairs[cart.id] = pair
                        self.miners_carts_associated.add(miner.id)
                        self.miners_carts_associated.add(cart.id)
                        break


    def get_next_move_for_unit(self, unit: Unit, home_base_position: Position, mines_positions: List[Position], my_crew: Crew):
        try:
            if unit.type == UnitType.MINER:
                return self.get_miner_move(unit, home_base_position, mines_positions, my_crew)
            if unit.type == UnitType.CART:
                return self.get_cart_move(unit, my_crew, home_base_position)
            if unit.type == UnitType.OUTLAW:
                return self.get_outlaw_action(unit, self.game_message)
            else:
                random_pos = self.get_random_position(self.game_message.map.get_map_size())
                return UnitAction(UnitActionType.MOVE, unit.id, random_pos)
        except Exception as e:
            print(e)
            traceback.print_exc()
            random_pos = self.get_random_position(self.game_message.map.get_map_size())
            return UnitAction(UnitActionType.MOVE, unit.id, random_pos)

    def get_miner_move(self, unit: Unit, home_base_position: Position, mines_positions: List[Position], my_crew: Crew):
        if len(mines_positions) != 0:
            closest_mine = find_closest(unit.position, mines_positions)
            distance_from_closest_mine = manathan_distance(unit.position, closest_mine)

            print(f"{unit.id} is in miner_carts_associated: {unit.id in self.miners_carts_associated}")

            if unit.blitzium >= 5 and unit.id in self.miners_carts_associated:
                cart_id = self.miner_cart_pairs[unit.id][CART_ID_KEY]
                cart = find_unit(self.units_dict, cart_id)
                cart_distance = manathan_distance(cart.position, unit.position)
                if (cart_distance == 1):
                    return UnitAction(UnitActionType.DROP, unit.id, cart.position)
                else:
                    return UnitAction(UnitActionType.MINE, unit.id, closest_mine)

            elif distance_from_closest_mine == 1 and (unit.blitzium < 5 or unit.id in self.miners_carts_associated):
                return UnitAction(UnitActionType.MINE, unit.id, closest_mine)

            elif distance_from_closest_mine > 1 and unit.blitzium < 5:
                unoccupied_closest_mine = find_unoccupied_closest(unit.position, mines_positions, self.game_message)
                closest = get_unoccupied_near_closest(unoccupied_closest_mine, self.game_message)
                return UnitAction(UnitActionType.MOVE, unit.id, closest)

            elif manathan_distance(unit.position, home_base_position) > 1 and unit.blitzium == 5:
                print("Miner trying to get to mine right now")
                closest = get_unoccupied_near_closest(home_base_position, self.game_message)
                return UnitAction(UnitActionType.MOVE, unit.id, closest)

            else:
                return UnitAction(UnitActionType.DROP, unit.id, home_base_position)
        else:
            closest = self.get_random_position(self.game_message.map.get_map_size())
            return UnitAction(UnitActionType.MOVE, unit.id, closest)

    def get_outlaw_action(self, unit: Unit, game_message: GameMessage):
        closestEnemyPosition: Position = getClosestEnemyByType(unit.position, None, game_message)

        if game_message.tick % 25 <= 5:
            random = self.get_random_position(self.game_message.map.get_map_size())
            return UnitAction(UnitActionType.MOVE, unit.id, random)

        if IsAdjacentToEnemy(unit.position, closestEnemyPosition) and game_message.get_crews_by_id()[game_message.crewId].blitzium > 150 and game_message.tick < 850:
            return UnitAction(UnitActionType.ATTACK, unit.id, closestEnemyPosition)
        else:
            position_to_go =  getNextOutlawMovePositionDumb(unit.position, game_message)
            return UnitAction(UnitActionType.MOVE, unit.id, position_to_go)

    def get_cart_move(self, cart: Unit, my_crew: Crew, home_base_position: Position):
        if cart.id in self.miners_carts_associated:
            units = my_crew.units
            cart_miner_pair = self.miner_cart_pairs[cart.id]
            miner_id = cart_miner_pair[MINER_ID_KEY]
            miner = find_unit(self.units_dict, miner_id)
            distance_from_miner = manathan_distance(cart.position, miner.position)
            distance_from_base = manathan_distance(cart.position, home_base_position)
            if distance_from_miner > 1 and cart.blitzium < 25:
                close_to_miner_position = get_unoccupied_near_closest(miner.position, self.game_message)
                return UnitAction(UnitActionType.MOVE, cart.id, close_to_miner_position)
            elif distance_from_base == 1 and cart.blitzium >= 25:
                return UnitAction(UnitActionType.DROP, cart.id, home_base_position)
            elif cart.blitzium >= 25:
                home_base_close = get_unoccupied_near_closest(home_base_position, self.game_message)
                return UnitAction(UnitActionType.MOVE, cart.id, home_base_close)
        else:
            depots_position = self.game_message.map.get_depots_positions()

            if len(depots_position) > 0:
                depot_closest = find_closest(cart.position, depots_position)
                closest_depot_unoccupied = get_unoccupied_near_closest(depot_closest, self.game_message)
                distance_from_depot_closest = manathan_distance(depot_closest, cart.position)
                if distance_from_depot_closest > 1 and cart.blitzium < 6:
                    return UnitAction(UnitActionType.MOVE, cart.id, closest_depot_unoccupied)
                elif distance_from_depot_closest == 1 and cart.blitzium < 6:
                    return UnitAction(UnitActionType.PICKUP, cart.id, depot_closest)
                elif manathan_distance(home_base_position, cart.position) == 1 and cart.blitzium >= 5:
                    return UnitAction(UnitActionType.DROP, cart.id, home_base_position)
                else:
                    home_base_close = get_unoccupied_near_closest(home_base_position, self.game_message)
                    return UnitAction(UnitActionType.MOVE, cart.id, home_base_close)
            else:
                if manathan_distance(home_base_position, cart.position) == 1 and cart.blitzium >= 5:
                    return UnitAction(UnitActionType.DROP, cart.id, home_base_position)
                elif cart.blitzium > 0:
                    home_base_close = get_unoccupied_near_closest(home_base_position, self.game_message)
                    return UnitAction(UnitActionType.MOVE, cart.id, home_base_close)
                else:
                    closest = self.get_random_position(self.game_message.map.get_map_size())
                    return UnitAction(UnitActionType.MOVE, cart.id, closest)

    def get_random_position(self, map_size: int) -> Position:
        return Position(random.randint(0, map_size - 1), random.randint(0, map_size - 1))
