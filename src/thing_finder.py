from typing import List, Dict
from game_message import TileType, Position, Map, Crew, GameMessage

from copy import deepcopy

def manathan_distance(currentPosition: Position, wantedPosition: Position):
    return abs(wantedPosition.x - currentPosition.x) + abs(wantedPosition.y - currentPosition.y)

# TODO: Find unoccupied closest
def find_closest(currentPosition: Position, positions: List[Position]):
    closest = Position(1000, 1000)
    min_position = 9999999999999999
    for position in positions:
        currentDistance = manathan_distance(currentPosition, position)
        if currentDistance < min_position:
            min_position = currentDistance
            closest = position

    return closest

def find_unoccupied_closest(currentPosition: Position, positions: List[Position], game_message: GameMessage):
    closest = Position(1000, 1000)
    min_position = 9999999999999999
    for position in positions:
        unoccupied_position = get_unoccupied_near_closest(position, game_message)
        has_unoccupied_adjacent_tiles = unoccupied_position.x != position.x or unoccupied_position.y != position.y
        if has_unoccupied_adjacent_tiles:
            currentDistance = manathan_distance(currentPosition, position)
            if currentDistance < min_position:
                min_position = currentDistance
                closest = position

    return closest

def find_tiles_of_type(map: Map, tile_type: TileType):
    tiles_of_type = []
    for i in range(map.get_map_size()):
        for j in range(map.get_map_size()):
            position = Position(i, j)
            if map.get_tile_type_at(position) == tile_type:
                tiles_of_type.append(position)
    return tiles_of_type

def get_unoccupied_near_closest(closest: Position, game_message: GameMessage):
    map = game_message.map
    crews = game_message.crews

    other_crews = [crew for crew in game_message.crews if crew.id != game_message.crewId]
    other_crew_base_positions = []
    for crew in other_crews:
        base_position = crew.homeBase
        for i in range(base_position.x-3, base_position.x+4):
            for j in range(base_position.y-3, base_position.y+4):
                other_crew_base_positions.append(Position(i, j))

    new_closest = deepcopy(closest)
    crews_units_positions = []
    for crew in crews:
        for unit in crew.units:
            crews_units_positions.append(unit.position)
    # Dans les y
    new_closest.y = new_closest.y + 1
    if (map.get_tile_type_at(new_closest)) == TileType.EMPTY and new_closest not in crews_units_positions and new_closest not in other_crew_base_positions:
        return new_closest
    new_closest.y = new_closest.y - 2
    if (map.get_tile_type_at(new_closest)) == TileType.EMPTY and new_closest not in crews_units_positions and new_closest not in other_crew_base_positions:
        return new_closest
    new_closest.y = new_closest.y + 1

    # Dans les x
    new_closest.x = new_closest.x + 1
    if (map.get_tile_type_at(new_closest)) == TileType.EMPTY and new_closest not in crews_units_positions and new_closest not in other_crew_base_positions:
        return new_closest
    new_closest.x = new_closest.x - 2
    if (map.get_tile_type_at(new_closest)) == TileType.EMPTY and new_closest not in crews_units_positions and new_closest not in other_crew_base_positions:
        return new_closest
    return closest

def get_number_of_tiles_near_unoccupied_near_closest(mines_position: List[Position], game_message: GameMessage) -> int:
    map = game_message.map
    crews = game_message.crews

    number_of_tiles = 0
    crews_units_positions = []
    for crew in crews:
        for unit in crew.units:
            crews_units_positions.append(unit.position)

    other_crews = [crew for crew in game_message.crews if crew.id != game_message.crewId]
    other_crew_base_positions = []
    for crew in other_crews:
        base_position = crew.homeBase
        for i in range(base_position.x-3, base_position.x+4):
            for j in range(base_position.y-3, base_position.y+4):
                other_crew_base_positions.append(Position(i, j))

    for mine_postion in mines_position:
        # Dans les y
        new_closest = deepcopy(mine_postion)
        new_closest.y = new_closest.y + 1
        if (map.get_tile_type_at(new_closest)) == TileType.EMPTY and new_closest not in crews_units_positions and new_closest not in other_crew_base_positions:
            number_of_tiles += 1
        new_closest.y = new_closest.y - 2
        if (map.get_tile_type_at(new_closest)) == TileType.EMPTY and new_closest not in crews_units_positions and new_closest not in other_crew_base_positions:
            number_of_tiles += 1
        new_closest.y = new_closest.y + 1

        # Dans les x
        new_closest.x = new_closest.x + 1
        if (map.get_tile_type_at(new_closest)) == TileType.EMPTY and new_closest not in crews_units_positions and new_closest not in other_crew_base_positions:
            number_of_tiles += 1
        new_closest.x = new_closest.x - 2
        if (map.get_tile_type_at(new_closest)) == TileType.EMPTY and new_closest not in crews_units_positions and new_closest not in other_crew_base_positions:
            number_of_tiles += 1

    return number_of_tiles
