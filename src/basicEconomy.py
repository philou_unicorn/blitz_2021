from game_message import GameMessage, Prices, UnitType
from map_cache import MapCache
from util import getOurUnitCounter
from thing_finder import get_number_of_tiles_near_unoccupied_near_closest

MAX_OUTLAW = 2
DO_NOT_BUY_STUFF_TICK = 850

def buy(currentMoney: int, prices: Prices, game_message: GameMessage, map_cache: MapCache) -> dict:
    ret = {UnitType.MINER: 0, UnitType.CART: 0, UnitType.OUTLAW: 0}
    ourCount = getOurUnitCounter(game_message)

    mines_position = map_cache.get_mines(game_message.map, game_message.tick)
    number_of_tiles_available = get_number_of_tiles_near_unoccupied_near_closest(mines_position, game_message)
    NUMBER_OF_MINERS_CARTS = number_of_tiles_available

    print("MAX NUMBER OF MINERS: " + str(NUMBER_OF_MINERS_CARTS))

    number_of_crew = len(game_message.crews)

    if number_of_crew <= 2:
        MAX_OUTLAW = 2
    else:
        MAX_OUTLAW = 1

    if game_message.tick > DO_NOT_BUY_STUFF_TICK and ourCount[UnitType.MINER] > 1:
        return ret

    if ourCount[UnitType.MINER] > 0 and prices.MINER + prices.CART + prices.OUTLAW < currentMoney and MAX_OUTLAW > ourCount[UnitType.OUTLAW]:
        ret[UnitType.OUTLAW] += (currentMoney - (prices.MINER + prices.CART)) // prices.OUTLAW

    if NUMBER_OF_MINERS_CARTS > 0 and ourCount[UnitType.MINER] == ourCount[UnitType.CART]:
        ret[UnitType.MINER] += 1
    elif NUMBER_OF_MINERS_CARTS > 0 and ourCount[UnitType.CART] < ourCount[UnitType.MINER] or (NUMBER_OF_MINERS_CARTS == 0 and ourCount[UnitType.CART] == ourCount[UnitType.MINER]):
        ret[UnitType.CART] += 1

    return ret
