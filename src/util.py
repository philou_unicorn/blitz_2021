from game_message import GameMessage, Crew, UnitType, Position, Map, Unit
from thing_finder import manathan_distance, get_unoccupied_near_closest
from typing import List, Dict

def getUnitCounter(crew_id: str, game_message: GameMessage):
    the_crew: Crew = game_message.get_crews_by_id()[crew_id]
    ret = {UnitType.MINER: 0, UnitType.CART: 0, UnitType.OUTLAW: 0}
    for unit in the_crew.units:
        ret[unit.type] += 1
    return ret

def getOurUnitCounter(game_message: GameMessage):
    return getUnitCounter(game_message.crewId, game_message)

def find_unit(units: Dict[str, Unit], unit_id) -> Unit:
    return units.get(unit_id)
    
def getOtherCrewPositionsByType(game_message: GameMessage, unitType: UnitType) -> List[Position]:
    ret = []
    crews = game_message.get_crews_by_id()
    for crew_id in crews:
        if crew_id != game_message.crewId:
            ret += [x.position for x in crews[crew_id].units if x.type == unitType or unitType == None]
    return ret

def getClosestEnemyByType(outlawPosition: Position, unitType: UnitType, game_message: GameMessage) -> Position:
    enemyPositions = getOtherCrewPositionsByType(game_message, unitType)
    ret: Position = None
    m_dist = 100000000
    for enemyPosition in enemyPositions:
        if ret != None:
            m_dist = manathan_distance(ret, outlawPosition)
        if manathan_distance(enemyPosition, outlawPosition) < m_dist:
            ret = enemyPosition
    if ret == None:
        ret = outlawPosition
    return ret

def getNearestEnemyEmptyTile(enemyPosition: Position, game_message: GameMessage) -> Position:
    return get_unoccupied_near_closest(enemyPosition, game_message)

def IsThereEnemyWithUnitType(game_message: GameMessage, unitType: UnitType):
    enemyOutlaws: int = 0
    crews = game_message.get_crews_by_id()
    for crew_id in crews:
        if crew_id != game_message.crewId:
            if getUnitCounter(crew_id, game_message)[unitType] > 0:
                return True
    return False

def getNextOutlawMovePositionDumb(outlawPosition: Position, game_message: GameMessage) -> Position:
    unitType: UnitType = UnitType.MINER
    if IsThereEnemyWithUnitType(game_message, UnitType.OUTLAW):
        unitType = UnitType.OUTLAW
    return getNearestEnemyEmptyTile(getClosestEnemyByType(outlawPosition, unitType, game_message), game_message)

def IsAdjacentToEnemy(outlawPosition: Position, enemyPosition: Position):
    return manathan_distance(outlawPosition, enemyPosition) == 1

def units_list_to_dict(units: List[Unit]) -> Dict[str, Unit]:
    return {unit.id: unit for unit in units}

