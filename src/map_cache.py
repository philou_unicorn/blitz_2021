from game_message import Map, TileType
from thing_finder import find_tiles_of_type

class MapCache:
    def __init__(self):
        self.mines = []

    def get_mines(self, daMap: Map, tick: int):
      if tick == 2:
          self.mines = find_tiles_of_type(daMap, TileType.MINE)
          return self.mines
      elif len(self.mines) > 0:
          return self.mines
      else:
        return self.mines