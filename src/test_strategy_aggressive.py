from game_message import Position, Unit, TileType, UnitType, GameMessage, Crew, Position, Rules
from strategy_aggressive import AggressiveStrategy
from unittest import TestCase, skip
from util import units_list_to_dict

class TestStrategyAggressive(TestCase):
    def test_associate_miner_with_cart(self):
        strategy = AggressiveStrategy()

        cart1 = Unit(
          1,
          UnitType.CART,
          5,
          Position(1, 1),
          []
        )

        cart2 = Unit(
          2,
          UnitType.CART,
          5,
          Position(1, 1),
          []
        )

        cart3 = Unit(
          3,
          UnitType.CART,
          5,
          Position(1, 1),
          []
        )

        miner1 = Unit(
          4,
          UnitType.MINER,
          5,
          Position(1, 1),
          []
        )

        miner2 = Unit(
          5,
          UnitType.MINER,
          5,
          Position(1, 1),
          []
        )

        miner3 = Unit(
          6,
          UnitType.MINER,
          5,
          Position(1, 1),
          []
        )

        units = [cart1, cart2, cart3, miner1, miner2, miner3]

        crew = Crew(
          "crew",
          "crew",
          Position(1, 1),
          10,
          10,
          units,
          [],
          Rules(10, 10, 10)
        )

        strategy.associate_miner_with_cart(crew)

        miner_cart_pair = {
          1: {
            "miner_id": miner1.id,
            'cart_id': cart1.id
          },
          2: {
            'miner_id': miner2.id,
            'cart_id': cart2.id
          },
          3: {
            'miner_id': miner3.id,
            'cart_id': cart3.id
          },
          4: {
            "miner_id": miner1.id,
            'cart_id': cart1.id
          },
          5: {
            'miner_id': miner2.id,
            'cart_id': cart2.id
          },
          6: {
            'miner_id': miner3.id,
            'cart_id': cart3.id
          }
        }

        self.assertEquals(strategy.miner_cart_pairs, miner_cart_pair)

    def test_handle_miner_and_cart_deaths(self):
        strategy = AggressiveStrategy()

        cart1 = Unit(
          1,
          UnitType.CART,
          5,
          Position(1, 1),
          []
        )

        cart2 = Unit(
          2,
          UnitType.CART,
          5,
          Position(1, 1),
          []
        )

        cart3 = Unit(
          3,
          UnitType.CART,
          5,
          Position(1, 1),
          []
        )

        miner1 = Unit(
          4,
          UnitType.MINER,
          5,
          Position(1, 1),
          []
        )

        miner2 = Unit(
          5,
          UnitType.MINER,
          5,
          Position(1, 1),
          []
        )

        miner3 = Unit(
          6,
          UnitType.MINER,
          5,
          Position(1, 1),
          []
        )

        units = [cart1, cart2, cart3, miner1, miner2, miner3]

        crew = Crew(
          "crew",
          "crew",
          Position(1, 1),
          10,
          10,
          units,
          [],
          Rules(10, 10, 10)
        )

        strategy.associate_miner_with_cart(crew)
        units_ded = [*units]
        units_ded.pop(0)
        units_ded_dict = units_list_to_dict(units_ded)
        strategy.handle_miner_and_cart_deaths(units_ded_dict)

        self.assertEquals(strategy.miners_carts_associated, {2, 3, 5, 6})
